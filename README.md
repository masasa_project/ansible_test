# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* install ansible

#### master node ####

* sudo apt-get install python3
* sudo apt-get isntall python3-pip
* sudo pip install ansible
* create directory in /etc/ansible 
* download the config file , you can search on google about config file , copy and create ansible.cfj file.
* create host file "touch hosts" an
* change the hostname if require in /etc/hostname
* create user ansadmin 
* give permissions to directory /etc/ansible
* enter this user to sudoer " visudo"
* change to the user and create key
* copy the pub key to connect slaves
* change in sshd_config in /etc/ssh/ the option the connect | PasswordAuthentication yes
* reset the service service sshd restart
 #### managed node ####
 * check hostname
 * create ansadmin user with same password
 * no need the create ssh !
 * change in sshd_config in /etc/ssh/ the option the connect | PasswordAuthentication yes
 * add user to sudor 
 

### connecte managed node to master####
* first change user to the ansadmin from master ,
* copy thet ssh file you create ssh-copy-id {host ip}
* try to login with ssh without password


### for single command use ad-hoc commands####
ansible {all} -m{module} {modulename} -a {command}
ping
command
stat 
yum
user 
setup


ansible all -m ping
ansible all -m yum -a "name=git"
ansible all -m user -a "name=john" -d
##add -d in the end to run as root
for choose specific inverntory file , use -i option , 
ansible all -m ping -i hosts
##default hosts file are in /etc/ansible/
how to get the list of hosts?
ansible all -i hosts --list-hosts
#####################################################################
#ansible Playbook
# ansible-playbook -i {invenotry location file} create_user.yml
#example
---
- name: this playbook for create user
  hosts: all
  become: true // equal to -d
  tasks:
  -  name: create user
     user:
       name: ansible_test

# equal to ansible all -m user -a "name=user"

#######################################################
#######################################################
#NOTE!
# state parameter:
# if we want to install app we use [installed,latest,present]
# if wwe want toremove app we use [absent,remove]

#ansible modules 
ls ./ansible
# if you want to run for test/check , run with parm --check in the end of command


#Gethring fact 
first task that run in ansible , 
going to retriv the system information for the machines .

#ansible vault
#create
ansible-vault create vault-id.yml
#decrypt | for remove the encrypt of this file 
ansible-vault decrypt vault-id.yml
#edit | change the value of the data
ansible-vault edit vault-id.yml
#view | to see the data
#encrypt | to encrypt back the value


git clone https://username:pass@bitbucket.org:masasa_project/ansible_test.git

#Roles
include , import , roles

#start and create roles
ansible-galaxy init {role_name}

# roles tree
./setup/
├── defaults
│   └── main.yml
├── files
├── handlers
│   └── main.yml
├── meta
│   └── main.yml
├── README.md
├── tasks
│   └── main.yml
├── templates
├── tests
│   ├── inventory
│   └── test.yml
└── vars
    └── main.yml

